from setuptools import setup

setup(
  name="translating_po_files",
  version="1.0.0",
  author='safa ',
  zip_safe=False,
  scripts=["po_file_translate.py"],
  # py_modules=['po_file_translate'],
  url="https://safahomsi@bitbucket.org/safahomsi/translation_po_files.git@po_file_translating",
)
