import argparse
import pathlib

import polib
from alive_progress import alive_it
from googletrans import Translator

# create parser
parser = argparse.ArgumentParser(description='python  script to auto translate po file  using google translate API ')

lang_choices = ["zh-TW", "zh-CN", "ur", "uk", "tr", "th", "te", "ta", "sw", "sv", "sr", "sl", "sk", "ru", "ro", "pt-PT",
                "pt-BR", "pl", "no", "nl", "ms", "mr", "ml", "lv", "lt", "ko", "kn", "ja", "iw", "it", "is", "id", "hu",
                "hr", "hi", "gu", "fr", "fil", "fi", "eu", "et", "es", "en-GB", "en", "el", "de", "da", "cy", "cs",
                "chr", "ca", "bn", "bg", "ar", "am"]


class ValidateLanguage(argparse.Action):
    """custom action to check if current lang code is validate """

    def __init__(self, option_strings, dest, help=None, **kwargs):
        super(ValidateLanguage, self).__init__(option_strings=option_strings, dest=dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):

        lang = self.dest
        if values in lang_choices:
            pass
        else:
            print("invalid language code you should use one of these codes :""\n", lang_choices)
            exit()

        setattr(namespace, self.dest, values)


# add arguments to the parser
parser.add_argument("po_file", type=pathlib.Path, help="add path of your po file which want to transle it")
parser.add_argument("lang", type=str, nargs="?", help="selected languge to trannslate  your file into ", default="ar",
                    action=ValidateLanguage)
parser.add_argument("-t", action="store_true", help="if you add this args the translation turn on ")
parser.add_argument("outputfile", type=pathlib.PurePath, help="add path to store translated po file ", )

# parse the arguments
args = parser.parse_args()


def po_translate(po_file, lang, t, outputfile):
    '''
    function that takes  2 parameters po file and destination language as str
    and it will return new file with the translated words.
    '''

    po_file_path = polib.pofile(po_file)
    translator = Translator()
    number_of_translated_entries = 0
    number_of_untranslated_entries = 0
    list_of_untrans_words = []
    # with alive_bar() as bar:
    for entry in alive_it(po_file_path):
        if len(entry.msgstr) == 0:
            try:

                if args.t and args.lang:

                    res = translator.translate(entry.msgid, dest=lang)
                    entry.msgstr = res.text
                    number_of_translated_entries += 1
                else:
                    number_of_translated_entries += 1
            except Exception as e:
                number_of_untranslated_entries += 1
                list_of_untrans_words.append(entry.msgid)
                print(f"Error {e}")

        else:
            # the word is translated.
            pass

    if args.t:
        # save the changes to the new file
        po_file_path.save(args.outputfile)
        if len(list_of_untrans_words) == 0:
            print(
                f"The file translated to {lang} successfully!, {number_of_translated_entries} words translated "
                f"successfully! "
                f",missing translation ={number_of_untranslated_entries}")
        else:
            print(
                f"The file translated to {lang} successfully!, {number_of_translated_entries} words translated "
                f"successfully! "
                f",missing translation ={number_of_untranslated_entries}, which msgid={list_of_untrans_words}")
    else:
        print(
            f"You have {number_of_translated_entries} untranslated word, if you would like to translate it, add -t "
            f"argument")


if __name__ == '__main__':
    po_translate(args.po_file, args.lang, args.outputfile, args.t)
