# overview:  <br>
helper script that open the po file selected by user then read empty translation(s), 
send them to google translate , get the translated string
and store it in new  file.

# how to install: <br>
**copy the following command:** <br>
pip install -e git+https://safahomsi@bitbucket.org/safahomsi/translation_po_files.git@master#egg=translating_po_files

## workflow:  <br>
1- get the po file as an input.  
2- the function will return the number of the untranslated words.  
3- we can pass optional argument in order to translate the file [-t].  
4- we can pass the output file name.
5- we can pass the language that we want to translate the file to (destination language).- [if the user insert invalid lang the script will notify him and will display the available languages].  
6- the script will report back to the user the number of translated words, and the number of failed translations.
-------------------------------------------

### syntax: <br>
po_file_translate.py [-h] [-t] po_file [lang] outputfile


<br>  
--------------------------------  
**Parameters**

| Name       | Type    | Description                                                      | Defualt | Required | Validation Rules                                 |  
|------------|---------|------------------------------------------------------------------|---------|----------|--------------------------------------------------|  
| po_file    | string  | path of user's po file which wants to transle it  .              |         | yes      | validate if selected file exists in current path |
| outputfile | string  | path of user's po file which wants to save translation inside it |         | yes      |                                                  |
| lang       | string  | destination language which user wants to translate it            | ar      | yes      | check if enterd lang code is valid               |
| -t         | boolean | if user passes it the file will be translated.                   | false   | no       |                                  |
-----------------------

### Supported Languages: <br>
**To see available supported language click on the following link:**
https://developers.google.com/admin-sdk/directory/v1/languages.